#!/bin/bash

echo "Qual HOSTNAME da maquina? "
read hostname
echo ${hostname} >> ~/Projects/Shell-Script/hostname.txt
echo "127.0.0.1 localhost.localdomain localhost" >> ~/Projects/Shell-Script/hostname.txt
echo "::1       localhost.localdomain localhost" >> ~/Projects/Shell-Script/hostname.txt
echo "127.0.1.1 ${hostname}.localdomain ${hostname}" >> ~/Projects/Shell-Script/hostname.txt
echo "Digite uma senha para a root: "
read password
echo ${password} >> ~/Projects/Shell-Script/password.txt
