#!/bin/bash

STATUS=$(cat /sys/class/power_supply/BAT1/status)
LEVEL=$(cat /sys/class/power_supply/BAT1/capacity)

if [[ $LEVEL -lt "20" ]]; then
  if [[ $STATUS == "Discharging" ]]; then
    notify-send --urgency=critical --expire-time=4000 --icon=battery-caution "Bateria Descarregando" "Está com ${LEVEL}%"

  fi
fi
