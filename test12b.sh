#!/bin/bash

packages() {
    clear
    cat <<EOF
[*] What's the packages you want to install?

[*] Choose one -
[1] Main packages
[2] Soft Packages

EOF

    read -p "[?] Select Option, typing 1 or 2: "

    if [[ $REPLY == "1" ]]; then
        echo "instalando packages main"
    elif [[ $REPLY == "2" ]]; then
        echo "instalando packages soft"
    else
        sleep 2 ; clear ; packages
    fi
}

videoDriverInstall(){
	cat <<- EOF

		[*] Want to install video drivers now?

		[*] Choose one -
		[1] Yes
		[2] No

	EOF

	read -p "[?] Select option: "

	if [[ $REPLY == "1" ]]; then
		cat <<- EOF
			[*] What's your video card?

			[*] Choose one -
			[1] Intel
			[2] Amd
			[3] Nvidia
		EOF

		read -p "[?] Select option: "

		if [[ $REPLY == "1" ]]; then
            echo "instalando intel"
		elif [[ $REPLY == "2" ]]; then
            echo "instalando amd"
		elif [[ $REPLY == "3" ]]; then
            echo "instalando nvidia"
		else
			echo "[!] Invalid Option: "
			sleep 3 ; clear ; videoDriverInstall
		fi
	elif [[ $REPLY == "2" ]]; then
		return 0
	fi
}

createPassword() {
    echo -e "Create a password for username: "
    unset password
    while IFS= read -r -s -n1 pass; do
        if [[ -z $pass ]]; then
            echo
            break
        else
            echo -n '*'
            password+=$pass
        fi
    done
    echo "Please, confirm it typing again: "
    unset confirm
    while IFS= read -r -s -n1 conf; do
        if [[ -z $conf ]]; then
            echo
            break
        else
            echo -n '*'
            confirm+=$conf
        fi
    done
    if [[ $password == $confirm ]]; then
        echo -e "[!] All Right! Continuing...\n"
        sleep 2
    else
        sleep 2 ; clear
        echo -e "[!] Wrong, please do it again\n"
        createPassword
    fi
}

createUser() {
    clear
    echo -e "Create a username: "
    read username
    createPassword
    echo ${username}
}
packages
videoDriverInstall
createUser
