#!/bin/bash

body() {
  read -p "Qual opção?"

  if [[ $REPLY == "1" ]]; then
    echo "Escolheu Inglês"
  elif [[ $REPLY == "2" ]]; then
    echo "Escolheu Português"
  else
    echo "opção invalida"
    body
  fi
  return $REPLY
}
body

ingles() {
  echo "Hello World!"
}

portugues() {
  echo "Olá mundo!"
}

if [[ $REPLY == "1" ]]; then
  ingles
elif [[ $REPLY == "2" ]]; then
  portugues
fi
