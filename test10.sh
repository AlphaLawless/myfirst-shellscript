#!/bin/bash

videoDriverInstall(){
	cat <<- EOF
		[*] Want to install video drivers now?

		[*] Choose one -
		[1] Yes
		[2] No
	EOF

	read -p "[?] Select option: "

	if [[ $REPLY == "1" ]]; then
		cat <<- EOF
			[*] What's your video card?

			[*] Choose one -
			[1] Intel
			[2] Amd
			[3] Nvidia
		EOF

		read -p "Select option: "

		if [[ $REPLY == "1" ]]; then
			echo "INTEL"
		elif [[ $REPLY == "2" ]]; then
			echo "AMD"
		elif [[ $REPLY == "3" ]]; then
			echo "NVIDIA"
		else
			echo "[!] Invalid Option: "
			clear ; videoDriverInstall
		fi
	elif [[ $REPLY == "2" ]]; then
		return 0
	fi
}
videoDriverInstall

continueInstall() {
	echo "Continue..."
}
continueInstall
