#!/bin/bash

shopt -s  extglob

usage() {
    cat <<EOF

usage: ${0##*/} [flags] [options]

    options:

    --install, -i
    --help, -h
    --version, -v

EOF
}

if [[ -z $1 || $1 = @(-h| --help) ]]; then
    usage
    exit $(( $# ? 0 : 1 ))
fi

version="${0##*/} version 1.0.0"

set_install() {
    get=$(ls $HOME/Projects/Shell-Script)

    if $get; then
        echo "install mbr"
    else
        echo "install uefi"
    fi
}

case "$1" in

    "--install"|"-i") set_install ;;
    "--version"|"-v") echo $version ;;
    "--help"|"-h") usage ;;
    *) echo "Invalid option" && usage ;;

esac

exit 0

