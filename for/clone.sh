#!/usr/bin/env bash

github=https://github.com
path=alphalawless/scripts

mkdir -p ~/clone
cd ~/clone

for i in {1..10}; do
    echo -e "\e[0;32mCloning the $i repository...\e[0m"
    git clone $github/$path clone$i 2>/dev/null
done

echo -e "\e[0;32mFinished! \e[0m"
