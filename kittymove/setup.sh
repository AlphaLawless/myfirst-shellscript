#!/usr/bin/env bash


get=$(ls kitty/themes)

if $get ; then
    mv ./themes kitty/themes
else
    mv ./kitty/themes/ kitty/themes.old
    mv ./themes/ kitty/themes
fi
