#!/bin/bash

option3() {
  echo "Please enter your password: "
  unset password
  while IFS= read -r -s -n1 pass; do
    if [[ -z $pass ]]; then
      echo
      break
    else
      echo -n '*'
      password+=$pass
    fi
  done
  echo "Please, confirm it again: "
  unset confirm
  while IFS= read -r -s -n1 conf; do
    if [[ -z $conf ]]; then
      echo
      break
    else
      echo -n '*'
      confirm+=$conf
    fi
  done
  if [[ $password == $confirm ]]; then
    echo -e "[!] All Right! Continue...\n"
    sleep 3
  else
    sleep 3
    clear
    echo -e "[!] Wrong, please do it again\n"
    option3
  fi
}
option3
