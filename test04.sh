#!/bin/bash

normal() {
  echo "Please enter your password: "
  read password
  sleep 1
  echo $password
}
normal

sleep 1

option1() {
  echo "Please enter your password: "
  stty -echo
  read password
  stty echo
  sleep 1
  echo ${password}
}
option1

sleep 1

option2() {
  echo "Please enter your password: "
  read -s password
  sleep 1
  echo $password
}
option2

sleep 1

option3() {
  echo "Please enter your password: "
  unset password
  while IFS= read -r -s -n1 pass; do
    if [[ -z $pass ]]; then
      echo
      break
    else
      echo -n '*'
      password+=$pass
    fi
  done
  echo ${password}
}
option3
