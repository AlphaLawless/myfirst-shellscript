#!/usr/bin/env bash

shopt -s extglob

usage() {
    cat <<EOF

usage: ${0##*/} [flags] [options]

    options:

    --base, -b
    --firewall, -f
    --install, -i
    --version, -v
    --help, -h
EOF
}

if [[ -z $1 || $1 = @(-h| --help) ]]; then
    usage
    exit $(( $# ? 0 : 1 ))
fi

version="${0##*/} version 1.0"

set_base() {
    cat <<EOF
    VEJA O QUE TEM DENTRO DE CADA BASE
    ...
EOF
}

set_install() {
    cat <<EOF
    ESCOLHA SEU SISTEMA

    [*] BASE MBR
    [*] BASE UEFI
EOF
}

set_firewall() {
    cat <<EOF
    VEJA O FIREWALL
EOF
}

case "$1" in

    "--base"|"-b") set_base ;;
    "--install"|"-i") set_install ;;
    "--firewall"|"-f") set_firewall ;;
    "--version"|"-v") echo $version ;;
    "--help"|"-h") usage ;;
    *) echo "Invalid option" && usage ;;
esac

exit 0
