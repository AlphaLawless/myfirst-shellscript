#!/bin/bash

body() {
	cat <<- EOF
		[*] What's the language which you want to start?

		[*] Choose one -
		[1] pt-BR...
		[2] en-US...

	EOF

  read -p "[?] Select one option, typing 1 or 2: "

  if [[ $REPLY == "1" ]]; then
    grub_install="\nColoque aqui o nome da partição do disco: "
  elif [[ $REPLY == "2" ]]; then
    grub_install="\nEnter the name of the disk partition here: "
  else
    echo -e "\n[!] Invalid Option!"
    exit 1
  fi

}
body

echo $grub_install
read partition_name
echo ${partition_name} >> ~/Projects/Shell-Script/test.txt
